package com.raywenderlich.galacticon;

import android.Manifest;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private Context ctx;
    private ImageView mItemImage;
    private TextView mItemDate;
    private TextView mItemDescription;
    private Photo mPhoto;
    private ImageRequester mImageRequester;
    private static final String PHOTO_KEY = "PHOTO";
    private static ImageView sItemImage;

    public PhotoHolder(View v,  final Context ctx) {
        super(v);
        this.ctx = ctx;
        mItemImage = (ImageView) v.findViewById(R.id.item_image);
        mItemDate = (TextView) v.findViewById(R.id.item_date);
        mItemDescription = (TextView) v.findViewById(R.id.item_description);
        mImageRequester = new ImageRequester(ctx);
        v.setOnClickListener(this);
        mItemImage.setOnClickListener(this);
        mItemImage.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(0, 1, 0, "Save as wallpaper").setOnMenuItemClickListener(contextMenu);
                if (Build.VERSION.SDK_INT >= 24) {
                    if (WallpaperManager.getInstance(ctx).isWallpaperSupported()) {
                        menu.add(0, 2, 1, "Save as lock screen").setOnMenuItemClickListener(contextMenu);
                    }
                }
                menu.add(0, 3, 2, "Save in storage").setOnMenuItemClickListener(contextMenu);
                menu.add(0, 4, 3, "Share").setOnMenuItemClickListener(contextMenu);
            }
        });
    }

    public static void setWallpaper(Context ctx) {
        WallpaperManager wallPaperManager = WallpaperManager.getInstance(ctx);
        try {
            Bitmap bitmapPhoto = ((BitmapDrawable)sItemImage.getDrawable()).getBitmap();
            wallPaperManager.setBitmap(bitmapPhoto);
        } catch (IOException e) { }
    }

    public static void setLockscreen(Context ctx) {
        try {
            Bitmap bitmapPhoto = ((BitmapDrawable)sItemImage.getDrawable()).getBitmap();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                WallpaperManager.getInstance(ctx).setBitmap(bitmapPhoto, null, true, WallpaperManager.FLAG_LOCK);
            }
        } catch (IOException e) { }
    }

    public static void setStorage(final Context ctx, final Boolean bshare) {
        PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult((Activity) ctx,
                new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, new PermissionsResultAction() {

                    @Override
                    public void onGranted() {
                        String extStorageDirectory = Environment.getExternalStorageDirectory().toString()+"/Pictures/Galacticon/";
                        FileOutputStream outS = null;
                        try {
                            Bitmap bitmapPhoto = ((BitmapDrawable)sItemImage.getDrawable()).getBitmap();
                            File filedir = new File(extStorageDirectory);
                            if(!filedir.exists()) filedir.mkdir();
                            File file = new File(filedir.getAbsolutePath(), "galacticon"+System.currentTimeMillis()+".jpg");
                            outS = new FileOutputStream(file);
                            bitmapPhoto.compress(Bitmap.CompressFormat.JPEG, 100, outS);
                            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            intent.setData(Uri.fromFile(file));
                            ctx.sendBroadcast(intent);Intent share = new Intent(Intent.ACTION_SEND);
                            if(bshare) {
                                share.setType("image/jpeg");
                                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(file+""));
                                ctx.startActivity(Intent.createChooser(share, "Share image"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (outS != null) {
                                    outS.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onDenied(String permission) {
                        Toast.makeText(ctx, "Sorry, we need the Storage Permission to do that", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public static void share(Context ctx) {
        setStorage(ctx, true);
    }

    private MenuItem.OnMenuItemClickListener contextMenu = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            sItemImage = mItemImage;
            MainActivity.tCtx = ctx;
            switch (item.getItemId()) {
                case 1:
                    MainActivity.setTask taskW = new MainActivity.setTask();
                    taskW.execute("Wallpaper");
                    break;
                case 2:
                    MainActivity.setTask taskL = new MainActivity.setTask();
                    taskL.execute("Lockscreen");
                    break;
                case 3:
                    MainActivity.setTask taskS = new MainActivity.setTask();
                    taskS.execute("Storage");
                    break;
                case 4:
                    MainActivity.setTask taskSh = new MainActivity.setTask();
                    taskSh.execute("Share");
            }
            return false;
        }
    };

    @Override
    public void onClick(View v) {
        Context context = itemView.getContext();
        Intent showPhotoIntent = new Intent(context, PhotoActivity.class);
        showPhotoIntent.putExtra(PHOTO_KEY, mPhoto);
        context.startActivity(showPhotoIntent);
    }

    public void bindPhoto(Photo photo) {
        mPhoto = photo;
        Picasso.with(mItemImage.getContext()).load(photo.getUrl()).into(mItemImage);
        mItemDate.setText(photo.getHumanDate());
        mItemDescription.setText(photo.getExplanation());
    }
}