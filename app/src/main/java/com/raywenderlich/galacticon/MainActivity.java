/*
* Copyright (c) 2016 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

package com.raywenderlich.galacticon;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import com.anthonycr.grant.PermissionsManager;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.app.AlarmManager.RTC_WAKEUP;

public class MainActivity extends AppCompatActivity implements ImageRequester.ImageRequesterResponse {
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private GridLayoutManager mGridLayoutManager;
    private RecyclerAdapter mAdapter;
    private ArrayList<Photo> mPhotosList;
    private ImageRequester mImageRequester;
    private Photo photoTmp;
    private int lastPosition = -1;
    private Boolean changeWPed = false;
    public static Boolean changeDM = false;
    private Boolean changeLO = false;
    private AlarmManager alarmMgr;
    public static Context tCtx;
    private SharedPreferences prefs;
    private PendingIntent pendingIntent;
    private MenuItem changeLayout, refreshWP, changeTheme;
    protected static Tracker mTracker;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        PermissionsManager.getInstance().notifyPermissionsChange(permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        changeLayout = menu.findItem(R.id.action_change);
        refreshWP = menu.findItem(R.id.action_refresh);
        changeTheme = menu.findItem(R.id.action_darkmode);

        loadActionMenu(true);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefs = this.getSharedPreferences("com.raywenderlich.galacticon", Context.MODE_PRIVATE);
        changeDM = prefs.getBoolean("com.raywenderlich.galacticon.changeDM", changeDM);
        changeLO = prefs.getBoolean("com.raywenderlich.galacticon.changeLO", changeLO);
        setThemeDM(changeDM);

        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        mTracker = analytics.newTracker(R.xml.global_tracker);
        mTracker.setScreenName("MainActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mGridLayoutManager = new GridLayoutManager(this, 2);
        mPhotosList = new ArrayList<>();
        mImageRequester = new ImageRequester(this);
        mAdapter = new RecyclerAdapter(mPhotosList);
        mRecyclerView.setAdapter(mAdapter);
        setRecyclerViewScrollListener();
        setRecyclerViewItemTouchListener();
        alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
    }

    private void loadActionMenu(Boolean fromStart) {
        changeLO = prefs.getBoolean("com.raywenderlich.galacticon.changeLO", changeLO);
        if(changeLO) {
            changeLayout.setTitle(R.string.ChangeLayoutLinear);
        } else {
            changeLayout.setTitle(R.string.ChangeLayoutGrid);
        }

        changeWPed = prefs.getBoolean("com.raywenderlich.galacticon.changeWPed", changeWPed);
        if(changeWPed) {
            refreshWP.setTitle(R.string.RefreshOff);
        } else {
            refreshWP.setTitle(R.string.RefreshOn);
        }

        changeDM = prefs.getBoolean("com.raywenderlich.galacticon.changeDM", changeDM);
        if(changeDM) {
            changeTheme.setTitle(R.string.normalmode);
        } else {
            changeTheme.setTitle(R.string.darkmode);
        }

        changeLayoutManager(changeLO);
        if(!fromStart)changeWPeveryday(changeWPed);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mPhotosList.size() == 0) {
            requestPhoto();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_change:
                changeLO = prefs.getBoolean("com.raywenderlich.galacticon.changeLO", changeLO);
                if(changeLO) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Layout - Linear")
                            .build());

                    prefs.edit().putBoolean("com.raywenderlich.galacticon.changeLO", false).apply();
                } else {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Layout- Grid")
                            .build());

                    prefs.edit().putBoolean("com.raywenderlich.galacticon.changeLO", true).apply();
                }
                loadActionMenu(true);
                break;
            case R.id.action_refresh:
                changeWPed = prefs.getBoolean("com.raywenderlich.galacticon.changeWPed", changeWPed);
                if(changeWPed) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Refresh off")
                            .build());

                    prefs.edit().putBoolean("com.raywenderlich.galacticon.changeWPed", false).apply();
                } else  {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Refresh on")
                            .build());

                    prefs.edit().putBoolean("com.raywenderlich.galacticon.changeWPed", true).apply();
                }
                loadActionMenu(false);
                break;
            case R.id.action_darkmode:
                changeDM = prefs.getBoolean("com.raywenderlich.galacticon.changeDM", changeDM);
                if(changeDM) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Normalmode")
                            .build());

                    prefs.edit().putBoolean("com.raywenderlich.galacticon.changeDM", false).apply();
                    Toast.makeText(MainActivity.this, "Darkmode - deactivated.", Toast.LENGTH_SHORT).show();
                } else {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Darkmode")
                            .build());

                    prefs.edit().putBoolean("com.raywenderlich.galacticon.changeDM", true).apply();
                    Toast.makeText(MainActivity.this, "Darkmode - activated.", Toast.LENGTH_SHORT).show();
                }
                finish();
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setThemeDM(Boolean changeDM) {
        if(!changeDM) {
            setTheme(R.style.AppTheme);
        } else {
            setTheme(R.style.AppDarkTheme);
        }
    }

    private void changeWPeveryday(Boolean changeWPed) {
        Intent serviceIntent = new Intent(MainActivity.this, PhotoService.class);
        serviceIntent.putExtra("URL", mImageRequester.urlRequest);
        MainActivity.this.startService(serviceIntent);

        Intent alarmIntent = new Intent(MainActivity.this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, 0);
        if(changeWPed) {
            Calendar cal_alarm = Calendar.getInstance();
            Calendar cal_now = Calendar.getInstance();
            //cal_alarm.set(Calendar.HOUR_OF_DAY, 5);
            //cal_alarm.set(Calendar.MINUTE, 0);
            //cal_alarm.set(Calendar.SECOND, 0);
            //if(cal_alarm.before(cal_now)) {
            //    cal_alarm.add(Calendar.MINUTE, 1);
            //}
            cal_alarm.add(Calendar.MINUTE, 1);
            alarmMgr.setInexactRepeating(RTC_WAKEUP, cal_alarm.getTimeInMillis(), 18000, pendingIntent);    // 1800s = 30min

            Toast.makeText(MainActivity.this, "Refresh - activated.", Toast.LENGTH_SHORT).show();
        } else if(!changeWPed) {
            stopService(serviceIntent);

            alarmMgr.cancel(pendingIntent);
            Toast.makeText(MainActivity.this, "Refresh - deactivated.", Toast.LENGTH_SHORT).show();
        }
    }

    public void changeWallpaperNotify() {
        NotificationManager notificationManager = (NotificationManager) MainActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
        ConnectivityManager connectivityManager = (ConnectivityManager) MainActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        Date time = new Date();
        SimpleDateFormat timeF = new SimpleDateFormat("HH:mm:ss");

        if(activeNetworkInfo != null) {
            mImageRequester = new ImageRequester(MainActivity.this);
            try {
                mImageRequester.getPhoto(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Notification notify = new Notification.Builder(MainActivity.this)
                    .setContentTitle("Wallpaper was set")
                    .setContentText("at "+timeF.format(time))
                    .setSmallIcon(android.R.drawable.ic_dialog_info)
                    .build();
            notificationManager.notify("refresh", 1, notify);
        } else {
            Notification notify = new Notification.Builder(MainActivity.this)
                    .setContentTitle("Wallpaper can't set")
                    .setContentText("at "+timeF.format(time))
                    .setSmallIcon(android.R.drawable.ic_dialog_info)
                    .build();
            notificationManager.notify("refresh", 2, notify);
        }
    }

    private void changeLayoutManager(Boolean changeLO) {
        if (changeLO) {
            mRecyclerView.setLayoutManager(mGridLayoutManager);
            findViewById(R.id.action_change);
            if (mPhotosList.size() == 1) {
                requestPhoto();
            }
        } else {
            mRecyclerView.setLayoutManager(mLinearLayoutManager);
        }
    }

    private int getLastVisibleItemPosition() {
        int itemCount;

        if (mRecyclerView.getLayoutManager().equals(mLinearLayoutManager)) {
            itemCount = mLinearLayoutManager.findLastVisibleItemPosition();
        } else {
            itemCount = mGridLayoutManager.findLastVisibleItemPosition();
        }

        return itemCount;
    }

    private void setRecyclerViewScrollListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int totalItemCount = mRecyclerView.getLayoutManager().getItemCount();
                if (!mImageRequester.isLoadingData() && totalItemCount == getLastVisibleItemPosition() + 1) {
                    requestPhoto();
                }
            }
        });
    }

    private void setRecyclerViewItemTouchListener() {

        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                if(swipeDir == 8) {
                    photoTmp = mPhotosList.get(position);
                    lastPosition = position;
                    mPhotosList.remove(position);
                    mRecyclerView.getAdapter().notifyItemRemoved(position);
                } else if(swipeDir == 4) {
                    if(lastPosition == -1 || photoTmp == null || mPhotosList.get(lastPosition).equals(photoTmp)) return;
                    mPhotosList.add(lastPosition, photoTmp);
                    mRecyclerView.getAdapter().notifyItemInserted(lastPosition);
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    private void requestPhoto() {
        try {
            mImageRequester.getPhoto(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receivedNewPhoto(final Photo newPhoto) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPhotosList.add(newPhoto);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    public static class setTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... param) {
            if(param[0].equals("Wallpaper")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("set Wallpaper")
                        .build());

                PhotoHolder.setWallpaper(tCtx);
                return "Wallpaper set";
            }
            if(param[0].equals("Lockscreen")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("set Lock screen")
                        .build());

                PhotoHolder.setLockscreen(tCtx);
                return "Lock screen set";
            }
            if(param[0].equals("Storage")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("Save in Storage")
                        .build());

                PhotoHolder.setStorage(tCtx, false);
                return "Saved in storage";
            }
            if(param[0].equals("Share")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("Share")
                        .build());

                PhotoHolder.share(tCtx);
                return "Share";
            }
            return "Try again...";
        }

        @Override
        protected void onPostExecute(String result) {
            if(!result.equals("Share")) Toast.makeText(tCtx, result, Toast.LENGTH_SHORT).show();
        }
    }
}