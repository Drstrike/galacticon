package com.raywenderlich.galacticon;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by drstrike on 19.07.17.
 */

public class PhotoService extends Service {

    private ImageRequester mImageRequester;
    private Intent intent;
    private MainActivity mMainActivity;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mImageRequester = new ImageRequester(this);
        mMainActivity = new MainActivity();

        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
        String urlFirst = intent.getStringExtra("URL");
        try {
            mImageRequester.getPhoto(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String urlSecond = mImageRequester.urlRequest;
        if(!urlFirst.equals(urlSecond)) {
            Toast.makeText(this, "service got a new Picture.", Toast.LENGTH_SHORT).show();
            mMainActivity.changeWallpaperNotify();
            try {
                mImageRequester.getPhoto(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Thread.sleep(6000); //600000
            Toast.makeText(this, "Ten minutes later...", Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Service.START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
