package com.raywenderlich.galacticon;

/**
 * Created by drstrike on 29.06.17.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AlarmReceiver extends BroadcastReceiver {

    private ImageRequester mImageRequester;

    @Override
    public void onReceive(Context context, Intent intent) {
        WakeLock.acquire(context);
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        Date time = new Date();
        SimpleDateFormat timeF = new SimpleDateFormat("HH:mm:ss");
        if(activeNetworkInfo != null) {
            mImageRequester = new ImageRequester(context);
            try {
                mImageRequester.getPhoto(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Notification notify = new Notification.Builder(context)
                    .setContentTitle("Wallpaper was set")
                    .setContentText("at "+timeF.format(time))
                    .setSmallIcon(android.R.drawable.ic_dialog_info)
                    .build();
            notificationManager.notify("refresh", 1, notify);
        } else {

            Notification notify = new Notification.Builder(context)
                    .setContentTitle("Wallpaper can't set")
                    .setContentText("at "+timeF.format(time))
                    .setSmallIcon(android.R.drawable.ic_dialog_info)
                    .build();
            notificationManager.notify("refresh", 2, notify);
        }
        WakeLock.release();
    }
}
